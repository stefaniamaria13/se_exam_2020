package exam.iunie;

public class MainS1 {
    class A {

    }

    class B extends D {
        private long t;

        public void b() {

        }
    }

    class C {

    }

    class D implements Y {

        public void met1(int i) {

        }
    }

    class E {
        public void met2() {

        }
    }

    class F {

    }

    interface Y {
        public void met1(int i);
    }
}
