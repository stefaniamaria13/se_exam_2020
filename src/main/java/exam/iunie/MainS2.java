package exam.iunie;

public class MainS2 {
    static class TaskExec extends Thread {
        private String nameTh;

        public TaskExec (String name){
            this.nameTh = name;
        }

        @Override
        public void run() {

            int numberOfMessage = 1;

            while (numberOfMessage < 6) {
                System.out.println(nameTh + " - " + numberOfMessage);
                numberOfMessage++;
                try {
                    sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Thread task1 = new TaskExec("StrThread1");
        Thread task2 = new TaskExec("StrThread2");
        task1.start();
        task2.start();
    }
}
